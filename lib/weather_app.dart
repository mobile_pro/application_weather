import 'package:flutter/material.dart';

void main() {
  runApp(WeatherApp());
}

class WeatherApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: buildAppBar(),
        body: buildBody(),
      ),

    );
  }
}

AppBar buildAppBar() {
  return AppBar(
    backgroundColor: Colors.transparent,
    elevation: 0,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.white,
      size: 30.0,
    ),
    title: Text(
      "Chanthaburi",
      style: TextStyle(
        color: Colors.black,
        fontWeight: FontWeight.bold,
        fontSize: 50,
      ),
    ),
    centerTitle: true,
  );
}

Widget buildBody() {
  return Stack(
    children: <Widget>[
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                'https://images.pexels.com/photos/4450406/pexels-photo-4450406.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1'),
            fit: BoxFit.cover,

          ),
        ),
        child: ListView(
          children: <Widget>[
            Container(
              height: 150,
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.only(top: 40.0),
              child: ListView(
                children: [
                  Text(
                    "29°",
                    style: TextStyle(
                      color: Colors.deepOrangeAccent,
                      fontSize: 100,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),

                ],
              ),
            ),
            Container(
              height: 200,
              child: ListView(
                children: <Widget>[
                  Container(
                    height: 180,
                    margin: EdgeInsets.all(10),
                    decoration: ShapeDecoration(
                      color: Colors.black54,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    child: ListView(
                      children: [
                        Container(
                          margin: EdgeInsets.all(30),
                        ),
                        Container(
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(right: 25),
                                ),
                                Container(
                                  child: Text(
                                    "Now",
                                    style: TextStyle(
                                        fontSize: 22, fontWeight: FontWeight.w600, color: Colors.white),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 53),
                                ),
                                Text(
                                  "02",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w600, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 36),
                                ),
                                Text(
                                  "03",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w600, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 36),
                                ),
                                Text(
                                  "04",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w600, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 36),
                                ),
                                Text(
                                  "05",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w600, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 32),
                                ),
                              ],
                            )
                        ),
                        Container(
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(right: 35),
                                ),

                                Container(
                                  child: Icon(
                                    Icons.cloud,
                                    color: Colors.indigoAccent,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 65),
                                ),
                                Icon(
                                  Icons.cloud,
                                  color: Colors.indigoAccent,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 37),
                                ),

                                Icon(
                                  Icons.cloud,
                                  color: Colors.indigoAccent,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 37),
                                ),

                                Icon(
                                  Icons.cloud,
                                  color: Colors.indigoAccent,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 37),
                                ),
                                Icon(
                                  Icons.sunny,
                                  color: Colors.deepOrange.shade500,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 32),
                                ),


                              ],
                            )
                        ),
                        Container(
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(right: 30),
                                ),
                                Container(
                                  child: Text(
                                    "26°",
                                    style: TextStyle(
                                        fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 55),
                                ),
                                Text(
                                  "28°",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 29),
                                ),
                                Text(
                                  "25°",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 29),
                                ),
                                Text(
                                  "27°",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 29),
                                ),
                                Text(
                                  "29°",
                                  style: TextStyle(
                                      fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 21),
                                ),

                              ],
                            )
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 450,
              margin: EdgeInsets.all(10),
              decoration: ShapeDecoration(
                color: Colors.black,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
              ),
              child: ListView(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    padding: EdgeInsets.all(5),
                  ),

                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Today",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),
                          Icon(
                            Icons.cloud,
                            color: Colors.indigoAccent,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "26°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Wed",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 75),
                          ),
                          Icon(
                            Icons.cloud,
                            color: Colors.indigoAccent,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "24°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Thu",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 80),
                          ),
                          Icon(
                            Icons.cloud,
                            color: Colors.indigoAccent,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "25°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Fri",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 93),
                          ),
                          Icon(
                            Icons.cloud,
                            color: Colors.indigoAccent,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "21°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "29°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Sat",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 86),
                          ),
                          Icon(
                            Icons.cloudy_snowing,
                            color: Colors.indigoAccent,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "21°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Sun",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 80),
                          ),
                          Icon(
                            Icons.sunny,
                            color: Colors.deepOrange.shade500,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "21°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Mon",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 74),
                          ),
                          Icon(
                            Icons.sunny,
                            color: Colors.deepOrange.shade500,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "22°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Tue",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 82),
                          ),
                          Icon(
                            Icons.cloud,
                            color: Colors.indigoAccent,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "23°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Wed",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 75),
                          ),
                          Icon(
                            Icons.cloud,
                            color: Colors.indigoAccent,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "23°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "30°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),
                  Divider(
                    color: Colors.grey.shade500,
                  ),
                  Container(
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                          ),

                          Container(
                            child: Text(
                              "Thu",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w300, color: Colors.white),

                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 80),
                          ),
                          Icon(
                            Icons.sunny,
                            color: Colors.deepOrange.shade500,
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "22°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 60),
                          ),

                          Text(
                            "32°",
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.w400, color: Colors.white),
                          ),
                        ],
                      )
                  ),

                ],
              ),
            ),
          ],
        ),
      ),
    ],
  );
}
